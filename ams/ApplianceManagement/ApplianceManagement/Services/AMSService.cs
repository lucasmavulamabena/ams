﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ApplianceManagement.Model;
using RestSharp;

namespace ApplianceManagement.Services
{
    public class AMSService
    {
        HttpWebRequest request;

        public AMSService()
        {
        }

        public static async Task<IRestResponse> SwitchAppliances(TenantApplianceModel tenantappliacne)
        {
            try
            {
                var TaskCompletionSource = new TaskCompletionSource<IRestResponse>();
                var client = new RestClient("http://192.168.43.144/AMS.Api");

                var request = new RestRequest("/api/ApplianceManagement", Method.GET);

                request.AddObject(tenantappliacne);

                client.ExecuteAsync(request, (IRestResponse response, RestRequestAsyncHandle handler) =>
                {
                    if (response.ResponseStatus == ResponseStatus.Error)
                    {
                        TaskCompletionSource.SetException(response.ErrorException);
                    }
                    else
                    {
                        TaskCompletionSource.SetResult(response);
                    }
                });

                return await TaskCompletionSource.Task;
            }
            catch (Exception e)
            {
                var message = e.Message;
                return null;
            }
        }

    }
}