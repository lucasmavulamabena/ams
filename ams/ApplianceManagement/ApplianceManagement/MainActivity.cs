﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using ApplianceManagement.Model;
using ApplianceManagement.Services;

namespace ApplianceManagement
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private Button _sendRequest;
        private ToggleButton _toggleGyser;
        private ToggleButton _toggleLights;
        private ToggleButton _toggleSprinkler;
        private ToggleButton _togglePlusg;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            _sendRequest = FindViewById<Button>(Resource.Id.btnSwitchAppliance);
            _toggleGyser = FindViewById<ToggleButton>(Resource.Id.toggleGyserOnOff);
            _toggleLights = FindViewById<ToggleButton>(Resource.Id.toggleLightsOnOff);
            _toggleSprinkler = FindViewById<ToggleButton>(Resource.Id.toggleSprinkerOnOff);
            _togglePlusg = FindViewById<ToggleButton>(Resource.Id.toggleplugsOnOff);

            _sendRequest.Click += delegate {
                SendRequestToSwitchAppliances();
            };

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;
        }

        public void SendRequestToSwitchAppliances()
        {
            var appliancestatuses = new TenantApplianceModel
            {
                TenantId = 1,
                Appliances = getAppliances()
            };

            var result = Task.Run(async () => await AMSService.SwitchAppliances(appliancestatuses)).Result;
        }

        public List<Appliance> getAppliances()
        {
            var appliances = new List<Appliance>();

            appliances.Add(new Appliance{Id = 1, StatusId = _toggleGyser.Checked ? 2 : 2});
            appliances.Add(new Appliance { Id = 2, StatusId = _toggleLights.Checked ? 2 : 1 });
            appliances.Add(new Appliance { Id = 3, StatusId = _toggleSprinkler.Checked ? 2 : 1 });
            appliances.Add(new Appliance { Id = 4, StatusId = _togglePlusg.Checked ? 2 : 1 });

            return appliances;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }
	}
}

