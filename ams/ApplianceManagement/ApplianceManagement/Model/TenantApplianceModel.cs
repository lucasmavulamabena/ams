﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ApplianceManagement.Model
{
    public class TenantApplianceModel
    {
        public int TenantId { get; set; }
        public List<Appliance> Appliances { get; set; }
    }

    public class Appliance
    {
        public int? Id { get; set; }
        public int StatusId { get; set; }
    }

}